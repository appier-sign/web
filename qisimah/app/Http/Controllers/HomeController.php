<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect('/');
    }

    public function landing()
    {
        return view('pages.guests.index');
    }

    public function about()
    {
        return view('pages.guests.about');
    }

    public function contact()
    {
        return view('pages.guests.contact');
    }

    public function login()
    {
        return view('pages.guests.login');
    }
}
