<html>
<head>
    @include('components.guest.head')
</head>
<body class="body-b">
<div class="richie-modal">
    <div class="div-block-24"><a href="#" class="richie-close w-inline-block" data-ix="modal-close"><img
                    src="{{ asset('images/qings.landing/close-icon.svg') }}" width="17.5"></a>
        <div style="padding-top:56.17021276595745%" id="w-node-dd8e2513-3547-ca23-fee7-a2142ac16903"
             class="w-video w-embed">
            <iframe width="560" height="315" src="https://www.youtube.com/embed/NsqI_ICmimY?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            {{--<iframe class="embedly-embed"--}}
                    {{--src="https://cdn.embedly.com/widgets/media.html?url=http%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3DI4rzedjcvMA&src=http%3A%2F%2Fwww.youtube.com%2Fembed%2FI4rzedjcvMA&type=text%2Fhtml&key=96f1f04c5f4143bcb0f2e68c87d65feb&schema=youtube"--}}
                    {{--scrolling="no" frameborder="0" allowfullscreen=""></iframe>--}}
        </div>
        <h3 class="heading-8">Richie Mensah</h3>
        <div class="text-block-5">LYNX Entertainment</div>
    </div>
</div>
<div class="form-modal">
    <div class="div-block-24">
        <a href="#" class="form-modal-close w-inline-block" data-ix="form-modal-close">
            <img src="{{ asset('images/qings.landing/close-icon.svg') }}" width="17.5">
        </a>
        <div class="form-block-3 w-form">
            <form id="qisimah-verification-form" name="email-form" data-name="Email Form" action="http://qisimahmail">
                <label for="name" class="verify-form-label">Name:</label>
                <input type="text" class="form-field w-input" maxlength="256" name="name" data-name="Name" placeholder="Enter your name" id="name">
                <label for="user-type" class="verify-form-label">User type:</label>
                <select id="user-type" name="user-type" data-name="User Type" class="form-field w-select">
                    <option value="">Select one...</option>
                    <option value="Recording Artist">Recording Artist</option>
                    <option value="Record Label / Company">Record Label / Company</option>
                    <option value="Advertiser">Advertiser</option>
                    <option value="Royalties Collection">Royalties Collection</option>
                </select>
                <label for="phone" class="verify-form-label">Phone Number</label>
                <input type="text" class="form-field w-input" maxlength="256" name="phone" data-name="Phone" placeholder="Kindly provide a number" id="phone" required="">
                <label for="email" class="verify-form-label">Email Address:</label>
                <input type="email" class="form-field w-input" maxlength="256" name="email" data-name="Email" placeholder="Enter your email" id="email" required="">
                <input type="submit" value="Submit" data-wait="Please wait..." class="submit-button-2 w-button">
            </form>
            <div class="w-form-done">
                <div>Thank you! Your submission has been received!</div>
            </div>
            <div class="w-form-fail">
                <div>Oops! Something went wrong while submitting the form.</div>
            </div>
        </div>
    </div>
</div>
<div class="w-embed w-iframe">
    <!--  Google Tag Manager (noscript)  -->
    <noscript>
        <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-52B53N9" height="0" width="0"
                style="display:none;visibility:hidden"></iframe>
    </noscript>
    <!--  End Google Tag Manager (noscript)  -->
    <div class="fb-customerchat" page_id="190832718139853" ref="website chatbot"></div>
</div>

@include('components.guest.navbar')

@yield('content')

<div class="trial">
    <div class="div-block-7">
        <!-- <h5 class="heading-3">04.</h5> -->
        <h5 class="heading-3">PACKAGES</h5>
    </div>
    <div class="w-row">
        <div class="w-col w-col-6">
            <h1 class="heading packagess" data-ix="fadebounce">Qisimah is available in four flexible packages.</h1>
            <h5 class="heading-3 payabe">*payable monthly</h5>
        </div>
        <div class="w-col w-col-6">
            <div class="row-5 w-row">
                <div class="column-4 w-col w-col-6">
                    <div class="text-block-3" data-ix="fadebounce">Basic</div>
                </div>
                <div class="w-col w-col-6">
                    <div class="text-block-3 standard" data-ix="fadebounce">Standard</div>
                </div>
            </div>
            <div class="w-row">
                <div class="column-4 w-col w-col-6">
                    <div class="text-block-3 premium" data-ix="fadebounce">Premium</div>
                </div>
                <div class="w-col w-col-6">
                    <div class="text-block-3 enterprise" data-ix="fadebounce">Enterprise</div>
                </div>
            </div>
            <a href="#" class="cta-hero-button reverse-button buttom w-button" data-ix="fadebounce">Try now</a></div>
    </div>
</div>


@yield('footer')

@include('components.guest.scripts')

</body>
</html>
