@extends('layouts.guest')

@section('content')
    <div class="login">
        <div class="div-block-20">
            <div class="form-block-2 w-form">
                <form action="{{ url('login') }}" method="post" id="qisimah-login-form" name="email-form" class="form" onsubmit="document.getElementById('submit-login').value='processing...';document.getElementById('submit-login').setAttribute('disabled', true);">
                    {{ csrf_field() }}
                    <label for="username" class="field-label-2">Email:</label>
                    <input type="email" class="form-field w-input" name="email" placeholder="name@email.com" id="username" required="">
                    <label for="password" class="field-label">Password</label>
                    <input type="password" class="form-field w-input" name="password" placeholder="******" id="password" required="">
                    <input type="submit" value="Login" id="submit-login" class="submit-button w-button">
                </form>
            </div>
        </div>
    </div>
@endsection