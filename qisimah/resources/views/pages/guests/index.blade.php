@extends('layouts.guest')
@section('content')
    <div class="header-cont">
        <div class="hero-content-parent">
            <div class="header-content">
                <h1 class="h1-display">Track whats being played on radio</h1>
                <div class="type-writer-cont">
                    <div class="seperator"></div>
                    <h2 id="typed" class="h2-display"></h2>
                    <div class="typed-cursor"></div>
                    <div class="typewrt-sign"></div>
                </div>
            </div>
            <div class="div-block" data-ix="load-text-2">
                <a href="#how-it-works" class="cta-hero-button w-button">How it works</a>
                <a href="https://api.whatsapp.com/send?phone=233506327253" target="_blank"
                   class="cta-hero-button reverse-button w-button">WhatsApp Us</a>
            </div>
        </div>
    </div>
    <div class="in-the-news">
        <div class="div-block-52"><a href="https://make-it-initiative.org/africa/cpt_digitalinno/qisimah/"
                                     target="_blank"
                                     class="in-the-news-link-block w-inline-block"><img
                        src="{{ asset('images/qings.landing/make-it-in-africa-trim.png') }}" width="80"></a><a
                    href="https://www.wired.it/economia/start-up/2018/05/30/startup-musica-primavera-sound-pro/"
                    target="_blank" class="in-the-news-link-block w-inline-block"><img
                        src="{{ asset('images/qings.landing/wired-bw.png') }}"
                        width="150"></a><a
                    href="https://pro.primaverasound.com/page/view/id/52" target="_blank"
                    class="in-the-news-link-block w-inline-block"><img
                        src="{{ asset('images/qings.landing/primavera.png') }}" width="100"></a><a
                    href="https://t.co/PkCGVlG1Si" class="in-the-news-link-block w-inline-block"><img
                        src="{{ asset('images/qings.landing/wsa_logo_2017-3.png') }}" width="150"></a><a
                    href="https://www.worldsummitawards.org/winner/qisimah-audio-insights/" target="_blank"
                    class="in-the-news-link-block w-inline-block"><img
                        src="{{ asset('images/qings.landing/wsa_logo_2017-1.png') }}" width="150"></a><a
                    href="http://disrupt-africa.com/2017/12/ghanas-qisimah-makes-waves-with-radio-content-monitoring/"
                    target="_blank" class="in-the-news-link-block w-inline-block"><img
                        src="{{ asset('images/qings.landing/wsa_logo_2017-2.png') }}"
                        width="150"></a><a
                    href="https://www.1millionstartups.com/" target="_blank"
                    class="in-the-news-link-block _11111 w-inline-block"><img
                        src="{{ asset('images/qings.landing/wsa_logo_2017-4.png') }}" width="150"></a>
        </div>
    </div>
    <div id="who-is-it-for" class="who-for">
        <div class="div-block-7">
            <!-- <h5 class="heading-3">01.</h5> -->
            <h5 class="heading-3">What is Qisimah?</h5>
        </div>
        <h1 class="heading who-for-use" data-ix="fadebounce">Single platform for monitoring and managing music and
            content
            plays on Radio.</h1>
    </div>
    <div class="horizontal-scroll-wrapper">
        <div data-duration-in="300" data-duration-out="100" class="tabs-2 w-tabs">
            <div class="tabs-menu w-tab-menu">
                <a data-w-tab="Tab 1" class="who-tab-links w-inline-block w-tab-link w--current">
                    <div class="individual-tooltip">
                        <div class="div-block-62"></div>
                        <div class="tooltip-text">Artists</div>
                    </div>
                    <div>Individual Artists</div>
                </a>
                <a data-w-tab="Tab 2" class="who-tab-links w-inline-block w-tab-link">
                    <div class="label-tooltip">
                        <div class="div-block-62"></div>
                        <div class="tooltip-text">Labels</div>
                    </div>
                    <div>Record Label</div>
                </a>
                <a data-w-tab="Tab 3" class="who-tab-links w-inline-block w-tab-link">
                    <div class="cmo-tooltip">
                        <div class="div-block-62"></div>
                        <div class="tooltip-text">CMOs</div>
                    </div>
                    <div>Collective Management</div>
                </a>
            </div>
            <div class="tabs-content w-tab-content">
                <div data-w-tab="Tab 1" class="who-tabs w-tab-pane w--tab-active">
                    <div class="all-who-wrapper">
                        <div class="image-wrapper">
                            <div class="user-type-number">
                                <div class="text-block-12">01</div>
                            </div>
                            <div class="div-block-50">
                                <h3 class="heading-10">Individual artist</h3>
                                <p class="paragraph-9">Do you want to know if your music is being played on the radio?
                                    Qisimah will give you live feedback on which station is playing your music. Our
                                    platform
                                    can help you identify areas of weakness in your music promotion plan. This will
                                    allow
                                    you to make the right adjustments and investments to gain exposure in order to go
                                    mainstream.</p>
                                <div class="div-block-51"><a href="#" class="link">SIGN UP NOW</a><img
                                            src="{{ asset('images/qings.landing/58cc2e0a9a7dd8aa76677481_arrow.png') }}"
                                            width="9" class="image-9"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div data-w-tab="Tab 2" class="who-tabs w-tab-pane">
                    <div class="all-who-wrapper">
                        <div class="image-wrapper label-record">
                            <div class="user-type-number">
                                <div class="text-block-12">02</div>
                            </div>
                            <div class="div-block-50">
                                <h3 class="heading-10">Management Firms / Record Labels</h3>
                                <p class="paragraph-9">Qisimah helps you analyse and compare your artists’performance
                                    against that of  others within the industry. With our credible audio analytics, you
                                    can
                                    make the right promotional decisions. We provide you with real time reports enabling
                                    you
                                    to track your artists’s performance continually and with ease.</p>
                                <div class="div-block-51"><a href="#" class="link">SIGN UP NOW</a><img
                                            src="{{ asset('images/qings.landing/58cc2e0a9a7dd8aa76677481_arrow.png') }}"
                                            width="9" class="image-9"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div data-w-tab="Tab 3" class="who-tabs w-tab-pane">
                    <div class="all-who-wrapper">
                        <div class="image-wrapper cmo">
                            <div class="user-type-number">
                                <div class="text-block-12">03</div>
                            </div>
                            <div class="div-block-50">
                                <h3 class="heading-10">Collective Management Organisations (CMOs)</h3>
                                <p class="paragraph-9">We provide you with an end-to-end solution that will enable you
                                    to
                                    effectively fulfill your mandate. Our user friendly platform will allow you to
                                    manage
                                    your processes of allocation, collection and distribution.</p>
                                <div class="div-block-51"><a href="#" class="link">SIGN UP NOW</a><img
                                            src="{{ asset('images/qings.landing/58cc2e0a9a7dd8aa76677481_arrow.png') }}"
                                            width="9" class="image-9"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section">
        <div id="how-it-works" class="how-wrapper">
            <div class="div-block-7">
                <!-- <h5 class="heading-3">02.</h5> -->
                <h5 class="heading-3">How does Qisimah work?</h5>
            </div>
            <div class="upload w-clearfix">
                <div class="image-holder-left"><img
                            src="{{ asset('images/qings.landing/Screen-Shot-2018-05-29-at-5.35.14-PM.png') }}"
                            width="959.5"
                            srcset="{{ asset('images/qings.landing/Screen-Shot-2018-05-29-at-5.35.14-PM-p-500.png') }} 500w, {{ asset('images/qings.landing/Screen-Shot-2018-05-29-at-5.35.14-PM.png') }} 1920w"
                            sizes="(max-width: 479px) 100vw, (max-width: 767px) 66vw, (max-width: 991px) 71vw, 55vw">
                </div>
                <div class="box-box"><img src="{{ asset('images/qings.landing/upload-new.svg') }}" class="how-icon">
                    <h4 class="forty-man">Upload</h4>
                    <p class="better-how">Upload your content to Qisimah&#x27;s database to start monitoring.</p>
                </div>
            </div>
            <div class="monitor w-clearfix">
                <div class="image-holder-right"><img src="{{ asset('images/qings.landing/summary.jpg') }}" width="960"
                                                     srcset="{{ asset('images/qings.landing/summary-p-500.jpeg') }} 500w, {{ asset('images/qings.landing/summary.jpg') }} 1920w"
                                                     sizes="(max-width: 479px) 100vw, (max-width: 767px) 66vw, (max-width: 991px) 71vw, 55vw">
                </div>
                <div class="box-box"><img src="{{ asset('images/qings.landing/monitor.svg') }}" class="how-icon">
                    <h4 class="forty-man">Monitor </h4>
                    <p class="better-how">Monitor detection statistics and analytics on content uploaded.</p>
                </div>
            </div>
            <div class="notifications-wrapper w-clearfix">
                <div class="image-holder-left"><img
                            src="{{ asset('images/qings.landing/Screen-Shot-2018-05-29-at-5.35.56-PM.png') }}"
                            width="960"
                            srcset="{{ asset('images/qings.landing/Screen-Shot-2018-05-29-at-5.35.56-PM-p-500.png') }} 500w, {{ asset('images/qings.landing/Screen-Shot-2018-05-29-at-5.35.56-PM-p-800.png') }} 800w, {{ asset('images/qings.landing/Screen-Shot-2018-05-29-at-5.35.56-PM-p-1080.png') }} 1080w, {{ asset('images/qings.landing/Screen-Shot-2018-05-29-at-5.35.56-PM.png') }} 1920w"
                            sizes="(max-width: 479px) 100vw, (max-width: 767px) 66vw, (max-width: 991px) 71vw, 55vw">
                </div>
                <div class="box-box"><img src="{{ asset('images/qings.landing/reports.svg') }}" class="how-icon">
                    <h4 class="forty-man">Notifications</h4>
                    <p class="better-how">Get real-time notification when your content is aired.</p>
                </div>
            </div>
            <div class="reports-wrapper w-clearfix">
                <div class="image-holder-left"><img
                            src="{{ asset('images/qings.landing/Screen-Shot-2018-05-29-at-5.38.26-PM.png') }}"
                            width="960"
                            srcset="{{ asset('images/qings.landing/Screen-Shot-2018-05-29-at-5.38.26-PM-p-500.png') }} 500w, {{ asset('images/qings.landing/Screen-Shot-2018-05-29-at-5.38.26-PM-p-800.png') }} 800w, {{ asset('images/qings.landing/Screen-Shot-2018-05-29-at-5.38.26-PM-p-1080.png') }} 1080w, {{ asset('images/qings.landing/Screen-Shot-2018-05-29-at-5.38.26-PM.png') }} 1920w"
                            sizes="(max-width: 479px) 100vw, (max-width: 767px) 66vw, (max-width: 991px) 71vw, 55vw">
                </div>
                <div class="box-box"><img src="{{ asset('images/qings.landing/upload-new.svg') }}" class="how-icon">
                    <h4 class="forty-man">Reports</h4>
                    <p class="better-how">Generate broadcast log and heat map on content uploaded.</p>
                </div>
            </div>
        </div>
        <div class="div-block-3">
            <div class="div-block-5">
                <div class="div-block-7">
                    <!-- <h5 class="heading-3">01.</h5> -->
                    <h5 class="heading-3">WHO IS QISIMAH FOR?</h5>
                </div>
                <h1 class="heading altd" data-ix="fadebounce">A single platform for the management and monitoring of
                    music
                    and content airplay.</h1>
                <div class="div-block-16">
                    <div class="cat-collection" data-ix="fadebounce">
                        <div class="div-block-12">
                            <h3 class="heading-4">Individual artist</h3>
                            <p class="new-paragraph">
                                Do you want to know if your music is being played on the radio?
                                Qisimah will give you live feedback on which station is playing your music. Our platform can
                                help you identify areas of weakness in your music promotion plan. This will allow you to make
                                the right adjustments and investments to gain exposure in order to go mainstream.
                            </p>
                        </div>
                    </div>
                    <div class="cat-collection" data-ix="fadebounce">
                        <div class="cat-img label"></div>
                        <div class="div-block-12">
                            <h3 class="heading-4">Artist Management Firms / Record Labels</h3>
                            <p class="new-paragraph">Qisimah helps you analyse and compare your artists’performance
                                against
                                that of  others within the industry. With our credible audio analytics, you can make the
                                right promotional decisions. We provide you with real time reports enabling you to track
                                your artists’s performance continually and with ease.</p>
                        </div>
                    </div>
                    <div class="cat-collection reverse-cow" data-ix="fadebounce">
                        <div class="cat-img royalty"></div>
                        <div class="div-block-12">
                            <h3 class="heading-4">Collective Management Organisations (CMOs)</h3>
                            <p class="new-paragraph">We provide you with an end-to-end solution that will enable you to
                                effectively fulfill your mandate. Our user friendly platform will allow you to manage
                                your processes of allocation, collection and distribution.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="div-block-2"></div>
    </div>

    @include('components.guest.get-started')

    <div id="case-studies" class="case-study">
        <div class="div-block-7">
            <!-- <h5 class="heading-3">03.</h5> -->
            <h5 class="heading-3">CASE STUDIES</h5>
        </div>
        <div class="div-block-53">
            <div class="div-block-54">
                <h1 class="heading altd sssswww" data-ix="fadebounce">What music industry players are saying about
                    Qisimah</h1>
            </div>
            <div class="div-block-55"><a href="#" class="link-block-5 w-inline-block" data-ix="modal-show"><img
                            src="{{ asset('images/qings.landing/RR-5.png') }}" class="testimonial-image">
                    <div class="div-block-48" data-ix="modal-show"><img
                                src="{{ asset('images/qings.landing/play-button.svg') }}" width="60"
                                class="playbutton"></div>
                    <div class="text-block-10">Richie Mensah</div>
                    <div class="text-block-11">CEO - LYNX Entertainment</div>
                </a><a href="#" class="link-block-5 w-inline-block"><img
                            src="{{ asset('images/qings.landing/c-the-nasty.png') }}"
                            srcset="{{ asset('images/qings.landing/c-the-nasty-p-500.png') }} 500w, {{ asset('images/qings.landing/c-the-nasty.png') }} 640w"
                            sizes="266px" class="testimonial-image">
                    <div class="div-block-48"><img src="{{ asset('images/qings.landing/play-button.svg') }}" width="60"
                                                   class="playbutton"></div>
                    <div class="text-block-10">Nasty C</div>
                    <div class="text-block-11">Artist</div>
                </a><a href="#" class="link-block-5 w-inline-block"><img
                            src="{{ asset('images/qings.landing/richie-add.png') }}"
                            srcset="{{ asset('images/qings.landing/richie-add-p-500.png') }} 500w, {{ asset('images/qings.landing/richie-add-p-800.png') }} 800w, {{ asset('images/qings.landing/richie-add.png') }} 1026w"
                            sizes="266px" class="testimonial-image"></a></div>
        </div>
    </div>
@endsection

@section('footer')
    @include('components.guest.footer')
@endsection