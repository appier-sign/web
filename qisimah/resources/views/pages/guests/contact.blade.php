@extends('layouts.guest')

@section('content')
    <div class="contact-hero">
        <div class="contact-page-hero">
            <div class="header-content">
                <div class="div-block-7">
                    <h5 class="heading-3">Contact us</h5>
                </div>
                <div class="w-row">
                    <div class="w-col w-col-4 w-col-small-small-stack"></div>
                    <div class="w-col w-col-4 w-col-small-small-stack">
                        <p class="white-paragraph-text">19 Banana Street
                            <br>Ambassadorial Enclave
                            <br>East Legon Accra, <br>Ghana</p>
                        <a href="https://www.google.com.gh/maps/place/Dropifi+Inc./@5.6444455,-0.1538623,17z/data=!3m1!4b1!4m5!3m4!1s0xfdf9b550b2ea9f7:0x54152149331c44d1!8m2!3d5.6444402!4d-0.1516736?hl=en" target="_blank" class="see-on-map w-inline-block w-clearfix"><img src="{{ asset('images/qings.landing/icons8-marker-filled-50.png') }}" width="25" class="image-7"><p class="paragraph-6 _4">View on map</p></a></div>
                    <div class="w-clearfix w-col w-col-4 w-col-small-small-stack">
                        <div class="text-block-13">Lets talk</div><img src="{{ asset('images/qings.landing/icons8-whatsapp.svg') }}" width="25" class="image-7">
                        <p class="white-paragraph-text">+233 50 632 7253</p>
                        <div class="lets-talk-spacer"></div><img src="{{ asset('images/qings.landing/icons8-ringer-volume-filled-50.png') }}" width="25" class="image-7">
                        <p class="white-paragraph-text">+233 50 723 0806</p><img src="{{ asset('images/qings.landing/icons8-ringer-volume-filled-50.png') }}" width="25" class="image-7">
                        <p class="white-paragraph-text">+233 24 966 3326</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-7">
        <div class="w-embed w-iframe"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1598.6001900384274!2d-0.15212536609455612!3d5.645180232951781!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xfdf9b557bcfbccf%3A0xc3dc2d256b854f8a!2sMEST+Incubator+Accra!5e0!3m2!1sen!2sgh!4v1527771827213" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen=""></iframe></div>
    </div>
    @endsection

@section('footer')
    @include('components.guest.footer')
@endsection