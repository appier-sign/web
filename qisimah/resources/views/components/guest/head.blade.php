<meta charset="utf-8">
<title>Qisimah | Real-time Audio Monitoring and Insights</title>
<meta content="Qisimah | Real-time Audio Monitoring and Insights" property="og:title">
<meta content="width=device-width, initial-scale=1" name="viewport">
<link href="{{ asset('css/qings.landing/normalize.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('css/qings.landing/webflow.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('css/qings.landing/qisimah.css') }}" rel="stylesheet" type="text/css">

<link href="{{ asset('images/qings.landing/favicon.gif') }}" rel="shortcut icon" type="image/x-icon">
<link href="{{ asset('images/qings.landing/webclip.png') }}" rel="apple-touch-icon">
<style>
    .gallery-container {
        position: -webkit-sticky !important;
        position: sticky !important;
    }
</style>