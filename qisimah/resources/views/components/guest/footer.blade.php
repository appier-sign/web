<div id="footer" class="footer">
    <div class="div-block-25">
        <div>
            <div class="w-row">
                <div class="column-6 w-col w-col-6"><img
                            src="{{ asset('images/qings.landing/qisimah-final-white-yellow.png') }}" width="180"
                            srcset="{{ asset('images/qings.landing/qisimah-final-white-yellow-p-500.png') }} 500w, {{ asset('images/qings.landing/qisimah-final-white-yellow-p-800.png') }} 800w, {{ asset('images/qings.landing/qisimah-final-white-yellow-p-1080.png') }} 1080w, {{ asset('images/qings.landing/qisimah-final-white-yellow-p-1600.png') }} 1600w, {{ asset('images/qings.landing/qisimah-final-white-yellow-p-2000.png') }} 2000w, {{ asset('images/qings.landing/qisimah-final-white-yellow-p-2600.png') }} 2600w, {{ asset('images/qings.landing/qisimah-final-white-yellow.png') }} 2888w"
                            sizes="(max-width: 479px) 100vw, 180px"></div>
                <div class="w-col w-col-6"></div>
            </div>
        </div>
        <div class="row-6 w-row">
            <div class="column-5 w-col w-col-3">
                <div class="footer-columns-heading _11 _12 follow-us">Follow US :</div>
                <div class="div-block-57"><a href="https://www.facebook.com/Qisimah/" target="_blank"
                                             class="social-icn w-inline-block"></a><a href="https://twitter.com/qisimah"
                                                                                      target="_blank"
                                                                                      class="social-icn twitter w-inline-block"></a><a
                            href="https://www.linkedin.com/company/qisimah/" target="_blank"
                            class="social-icn linkedin w-inline-block"></a><a
                            href="https://www.linkedin.com/company/qisimah/" target="_blank"
                            class="social-icn instagram w-inline-block"></a></div>
            </div>
            <div class="w-col w-col-3">
                <div class="footer-columns-heading _11 _12">ProductS</div>
                <a href="#" class="footer-page-link">Qisimah Audio Monitoring</a><a href="#" class="footer-page-link">Music
                    Airplay Charts</a><a href="#"
                                         class="footer-page-link w-hidden-main w-hidden-medium w-hidden-small w-hidden-tiny">Ad
                    Monitoring</a></div>
            <div class="column-12 w-col w-col-3">
                <div class="footer-columns-heading _11">Company</div>
                <a href="#" class="footer-page-link">About</a><!--<a href="#" class="footer-page-link">Jobs</a>--><a
                        href="#" class="footer-page-link">Privacy Policy</a></div>
            <div class="w-col w-col-3">
                <div class="footer-columns-heading">learn / support</div>
                <!--<a href="#" class="footer-page-link">FAQ</a>--><a href="#" class="footer-page-link">Contact
                    Support</a><!--<a href="#" class="footer-page-link">Brand</a>--></div>
        </div>
        <div class="div-block-17">
            <div>
                <div class="text-block-4">©2018 Qisimah. All Rights Reserved.</div>
            </div>
            <div class="div-block-18">
                <div>Broadcast Monitoring Powered by:</div>
                <img src="{{ asset('images/qings.landing/acrcloud-logo.png') }}" width="100" class="image-6"></div>
        </div>
    </div>
</div>