<div id="cta-main" class="cta">
    <div class="div-block-26">
        <div class="div-block-28">
            <h3 class="heading-9">Ready to get started?</h3>
            <div class="text-block-6">Get verified to see Qisimah in action</div>
        </div>
        <div class="div-block-27">
            <a href="https://api.whatsapp.com/send?phone=233506327253" target="_blank" class="cta-hero-button getverify-bttn w-button">Get started</a>
        </div>
    </div>
</div>