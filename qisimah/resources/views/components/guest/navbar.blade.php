<div data-collapse="medium" data-animation="default" data-duration="400" class="navbar w-nav">
    <a href="{{ route('welcome') }}" class="w-nav-brand"><img src="{{ asset('images/qings.landing/Qisimah-logo.gif') }}" width="150"></a>
    <nav role="navigation" class="nav-menu w-nav-menu">
        <a href="{{ route('welcome') }}" class="nav-link w-nav-link w--current">Welcome</a>
        {{--<a href="{{ url('about-us') }}" class="nav-link w-nav-link">About</a>--}}
        <a href="{{ route('welcome') }}#how-it-works" class="nav-link w-nav-link">How it works</a>
        <a href="https://www.qisimah.com/chart" class="nav-link w-nav-link">Charts</a>
        <a href="{{ url('contact-us') }}" class="nav-link w-nav-link">Contact</a>
        {{--<a href="login.html" class="login-button w-button">SIGN UP</a>--}}
        <a href="{{ url('log-in') }}" class="button w-button">LOGIN</a>
    </nav>
    <div class="menu-button w-nav-button">
        <div class="w-icon-nav-menu"></div>
    </div>
</div>