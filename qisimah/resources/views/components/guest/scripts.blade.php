<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js" type="text/javascript"></script>
<script type="text/javascript">WebFont.load({google: {families: ["Karla:regular,italic,700,700italic"]}});</script>
<!-- [if lt IE 9]>
<script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script>
<![endif] -->
<script type="text/javascript">!function (o, c) {
        var n = c.documentElement, t = " w-mod-";
        n.className += t + "js", ("ontouchstart" in o || o.DocumentTouch && c instanceof DocumentTouch) && (n.className += t + "touch")
    }(window, document);</script>
<script src="https://code.jquery.com/jquery-3.3.1.min.js" type="text/javascript"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="{{ asset('js/qings.landing/webflow.js') }}" type="text/javascript"></script>
<!-- [if lte IE 9]>
<script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->
<!--  Global site tag (gtag.js) - Google Analytics  -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-109589703-1"></script>

<script src="https://mattboldt.com/demos/typed-js/js/typed.custom.js"></script>
<script>
    $(function () {
        $("#typed").typed({
            strings: ["with Qisimah", "wherever", "whenever!"],
            typeSpeed: 50,
            startDelay: 1000,
            backSpeed: 200,
            backDelay: 4000,
            loop: true,
            loopCount: Infinity,
            /*callback: function(){
              shift();
            }*/
        });
    });
</script>